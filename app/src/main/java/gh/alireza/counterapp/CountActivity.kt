package gh.alireza.counterapp

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import gh.alireza.counterapp.Constants.COUNT_NUM
import gh.alireza.counterapp.databinding.ActivityCountBinding

class CountActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCountBinding
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCountBinding.inflate(layoutInflater)
        setContentView(binding.root)




        binding.textViewCount.text = num.toString()



        binding.imageButton.setOnClickListener {
            num++


            binding.textViewCount.text = num.toString()


        }
    }
    private var num: Int = 0
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.i("MyTag", "onSaveInstanceState")


        outState.putInt(COUNT_NUM, num)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        Log.i("MyTag", "onRestoreInstanceState")

        num = savedInstanceState.getInt(COUNT_NUM)
    }




}

